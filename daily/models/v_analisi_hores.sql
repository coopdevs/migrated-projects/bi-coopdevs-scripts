{{ config(materialized='table') }}

  SELECT
  p.id,
    p.name,
    p.active,
    pt2.name AS task,
    pt2.date_start::timestamp without time zone as date_start,
    pt2.date_end::timestamp without time zone as date_end,
    ptt.name AS stage,
    a.name AS activity_name,
    a.date::timestamp without time zone as date,
    date_part('week', a.date) as week,
    concat(date_part('dow', a.date),'-',to_char(a.date, 'Day')) as week_Day,
    a.unit_amount AS hores,
    hores_persona_dia,
    round(a.unit_amount * 60::double precision) AS minuts,
    a.create_date at time zone 'utc' at time zone 'Europe/Madrid' as data_hora_inici_tasca,
    (a.create_date+concat(a.unit_amount::varchar,' hours')::interval)  at time zone 'utc' at time zone 'Europe/Madrid' as data_hora_fi_tasca,
    row_number() over (partition by e.name, a.date order by a.create_date desc) as ordre_tasques_invers,
    case when row_number() over (partition by e.name, a.date order by a.create_date desc)=1 then (a.create_date at time zone 'utc' at time zone 'CET')::time else null end as hora_inici_ultima_tasca,
    case when row_number() over (partition by e.name, a.date order by a.create_date desc)=1 then ((a.create_date+concat(a.unit_amount::varchar,' hours')::interval) at time zone 'utc' at time zone 'CET')::time else null end as hora_fi_ultima_tasca,
    e.name AS employee,
    aac.name AS account_name,
    rp.name AS partner_name
    ,aag."name" as group_name_lvl2, coalesce(aagp."name",aag."name")  as group_name_lvl1, aag.complete_name as group_complete_name
    ,date_part('hour', (a.create_date at time zone 'utc' at time zone 'Europe/Madrid')::time)+ date_part('minutes', (a.create_date at time zone 'utc' at time zone 'Europe/Madrid')::time)/60.0 as hora_inici
    ,date_part('hour', ((a.create_date+concat(a.unit_amount::varchar,' hours')::interval) at time zone 'utc' at time zone 'Europe/Madrid')::time )+ date_part('minutes', ((a.create_date+concat(a.unit_amount::varchar,' hours')::interval) at time zone 'utc' at time zone 'Europe/Madrid')::time )/60.0 as hora_fi
    ,a.date::varchar as date_str
  FROM odoo12_project_project p
    JOIN odoo12_account_analytic_line a ON a.project_id = p.id
    JOIN odoo12_hr_employee e ON e.id = a.employee_id
    join (select date, employee_id, sum(a.unit_amount) as hores_persona_dia
		  from odoo12_account_analytic_line a
		  group by date, employee_id) x on x.date=a."date" and x.employee_id=a.employee_id
    left JOIN odoo12_project_task pt2 ON pt2.project_id = p.id AND pt2.id = a.task_id
    LEFT JOIN odoo12_project_task_type ptt ON pt2.stage_id = ptt.id
    LEFT JOIN odoo12_account_analytic_account aac ON p.analytic_account_id = aac.id
    LEFT JOIN odoo12_res_partner rp ON rp.id = aac.partner_id
    left join odoo12_account_analytic_group aag on aac.group_id =aag.id
    left join odoo12_account_analytic_group aagp on aag.parent_id=aagp.id