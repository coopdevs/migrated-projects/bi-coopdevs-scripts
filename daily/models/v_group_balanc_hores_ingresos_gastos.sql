{{ config(materialized='table') }}

select group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name, date, partner_name, sum(debit) as debit, sum(credit) as credit, sum(hores) as hores
from (
select group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name, date, partner_name,sum(debit) as debit, sum(credit) as credit, 0 as hores
from v_apuntes_contables_reviewed
group by group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name,date, partner_name
union all
select group_name_lvl2, group_name_lvl1, group_complete_name, account_name, date, partner_name, 0,0, sum(hores) as hores
from  public.odoo12_project_tasks
group by group_name_lvl2, group_name_lvl1, group_complete_name, account_name, date, partner_name
) a
group by group_name_lvl2, group_name_lvl1, group_complete_name, analyic_account_name, date, partner_name